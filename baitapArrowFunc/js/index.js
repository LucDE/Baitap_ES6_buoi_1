const color_content = document.querySelector("#colorContainer");
const colorList =  ["pallet","viridian","pewter","cerulean",
"vermillion","lavender","celadon","saffron","fuschia","cinnabar"];
const house = document.querySelector("#house");
let loadButton = () => {
    var list_button = ``;
    let button = colorList.map(item => `<button class = "color-button ${item}
    " id="${item}" onclick="chooseColor('${item}')"></button>` );
    for (let item of button){
        list_button+=item;
    }
    color_content.innerHTML = list_button;
}
let chooseColor = (id) => {
    house.className = `house ${id}`;
    loadButton();
    document.getElementById(id).classList.add('active');
}
loadButton();
document.querySelector("#pallet").click();


