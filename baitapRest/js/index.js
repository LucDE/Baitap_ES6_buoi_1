document.querySelector("#btnKhoi1").onclick = () => {
  let _diemToan = document.querySelector("#inpToan").value * 1;
  let _diemLy = document.querySelector("#inpLy").value * 1;
  let _diemHoa = document.querySelector("#inpHoa").value * 1;
  tinhDTB(_diemToan, _diemLy, _diemHoa);
  let dtb_khoi1 = tinhDTB(_diemToan,_diemLy ,_diemHoa);
  document.querySelector("#tbKhoi1").innerHTML = dtb_khoi1;
};
document.querySelector("#btnKhoi2").onclick = () => {
  let _diemVan = document.querySelector("#inpVan").value * 1;
  let _diemSu = document.querySelector("#inpSu").value * 1;
  let _diemDia = document.querySelector("#inpDia").value * 1;
  let _diemAnh = document.querySelector("#inpEnglish").value * 1;
  let dtb_khoi2 = tinhDTB(_diemVan, _diemSu, _diemDia, _diemAnh);
  document.querySelector("#tbKhoi2").innerHTML = dtb_khoi2;
};
let tinhDTB = (...resParam) => {
    let tongdiem = 0;
    let diemTB = 0;
    for (let diem of resParam) {
      tongdiem += diem;
    }
    diemTB = tongdiem / resParam.length;
    return diemTB;
  };